program bingo;

consts:
	MinJug=1;
	Numjugadores=3;
	Nfilas=3;
	Ncolumnas=5;
	Nummin=1;
	Nummax=100;
	Tachar="XX";
	Blanco=" ";
	Minbolas=0;
	Maxbolas=400;
	Mincartones=0;
	Maxcartones=400;
	
types:
	
	TipoNumFilas=int 1..Nfilas;
	TipoNumColumnas=int 1..Ncolumnas;
	TipoNumBolas=int Minbolas..Maxbolas;
	TipoNumCartones=int Mincartones..Maxcartones;

	TipoColumna = int Nummin..Nummax;
	TipoNumsC = array [TipoNumColumnas] of TipoColumna;
	TipoTachado= array [TipoNumColumnas] of bool;

	TipoEntrada=(Rojo, Verde, Azul, Amarillo, FIN);
	TipoColor= TipoEntrada Rojo..Amarillo;

	TipoBola = record
	{
		coloor: TipoColor;
		numeros: TipoColumna;
	};
	TipoBolas = array [TipoNumBolas] of TipoBola;
	TipoExtraccion = record
	{
		bolaas: TipoBolas;
		nbolas: int;
	};


	TipoFila = record
	{
		coloor: TipoColor;
		columnas: TipoNumsC;
		tachadoo: TipoTachado;
	};
	TipoFilas = array [TipoNumFilas] of TipoFila;
	TipoCarton = record
	{
		filas: TipoFilas;
		nfilas: TipoNumFilas;
	};
	TipoCartones = array [TipoNumCartones] of TipoCarton;
	TipoJugador = record
	{
		cartones: TipoCartones;
		ncartones: int;
	};
	TipoJugadores = array [MinJug..Numjugadores] of TipoJugador;


procedure leerextraccion(ref bola: TipoBola)
	car: char;
	i: int;
{
	if(not eof()){
		saltarblancos();
		peek(car);
		read(bola.coloor);
	}
	if(not eof()){
		saltarblancos();
		peek(car);
		read(bola.numeros);
	}
}
procedure tacharfila(ref fila: TipoFila, bola: TipoBola,ref tachado: bool)
	i: int;
{
	for(i=1, i <= Ncolumnas){
		if(fila.coloor==bola.coloor and fila.columnas[i]==bola.numeros){
			tachado=True;
			fila.tachadoo[i]=True;
		}else{
			tachado=False;
			fila.tachadoo[i]=False;
		}
	}	
}
procedure tacharcarton(ref carton: TipoCarton, bola: TipoBola)
	i: int;
	tachado: bool;
{
	for(i=1, i <= Nfilas){
		tacharfila(carton.filas[i],bola,tachado);
	}
}
procedure tacharcartones(ref jugador: TipoJugadores, bola: TipoBola)
	i: int;
	j: int;
{
	for(i=1,i <= Numjugadores){
		for(j=1,j <= jugador[i].ncartones){
			tacharcarton(jugador[i].cartones[j],bola);
		}
	}
}
function linea(fila: TipoFila, esta: bool): bool
	i: int;		
{
	esta=True;
	i=1;
	while(i <= Ncolumnas and esta){
		if(not fila.tachadoo[i]){
			esta=False;	
		}else{
			esta=True;
			i=i+1;
		}
	}
	return esta;
}
function bingocarton(cart: TipoCarton, esta: bool): bool
	i: int;
{
	esta=True;
	i=1;
	while(i <= Nfilas and esta){
		if(not linea(cart.filas[i],esta)){
			esta=False;	
		}else{
			esta=True;
			i=i+1;
		}
	}
	return esta;
}
function bingodeljugador(jugador: TipoJugador, sale: bool): bool
	i: int;
{
	sale=False;
	i=1;
	while(i <= jugador.ncartones and not sale){
		if(bingocarton(jugador.cartones[i],sale)){
			sale=True;
			i=i+1;	
		}else{
			sale=False;
		}
	}
	return sale;
}
function ganador(jugador: TipoJugadores, esta: bool): bool
	i: int;
	j: int;
{
	i=1;
	for(i=1,i <= Numjugadores){
		if(bingodeljugador(jugador[i],esta)){
			esta=True;	
		}else{
			esta=False;
		}
	}
	return esta;
}








/*
procedure estadocartones(ref jugador: TipoJugadores, bolas: TipoBola, ref esta: bool)
	i: int;
	j: int;
	ntachados: int;
{
	ntachados=0;
	for(i=1,i <= Numjugadores){
		for(j=1,j <= jugador[i].ncartones){
			if(tachar(jugador[i].carton.filas,bolas) == True){
			ntachados = ntachados+1;
				if(bingodeljugador(jugador[i],esta)){
					writeln("Bingo");
				}else{
					writeln("Tachado");
				}
			}else{
				writeln("Nada");
			}

		}
	}
}	
*/









procedure escribircolumnas(ref fila: TipoFila)
	i: int;
{
	for(i=1, i <= Ncolumnas){
		if(fila.tachadoo[i]){
			write(Tachar);
		}else{
			write(fila.columnas[i]);
		}
	write(Blanco);
	}
}
procedure escribircolor(ref fila: TipoFila)
	i: int;
{
	write(fila.coloor);
}
procedure escribircarton(ref cart: TipoCarton)
	i: int;
{
	for(i=1, i <= Nfilas){
		escribircolor(cart.filas[i]);
		write(Blanco);
		escribircolumnas(cart.filas[i]);
		writeeol();
	}
}
procedure intercambiarnums(ref num1: TipoColumna, ref num2: TipoColumna)
	aux: TipoColumna;
{
	aux=num1;
	num1=num2;
	num2=aux;
}
procedure ordenarnumeros(ref num: TipoNumsC)
	i: int;
	j: int;
	min: TipoColumna;
{	
	min=num[1];
	for(i=1, i < Ncolumnas){
		for(j=1, j <= Ncolumnas){
			if(num[j] < min){	
				intercambiarnums(num[j],num[i]);
			}
		}
	}
}
procedure ordenarcarton(ref carton: TipoCarton)
	i: int;
{
	for(i=1, i <= Nfilas){
		ordenarnumeros(carton.filas[i].columnas);
	}
	escribircarton(carton);
	writeeol();
}
procedure ordenarcartones(ref jugador: TipoJugadores)
	i: int;
	j: int;
{
	for(i=1,i <= Numjugadores){
		for(j=1,j <= jugador[i].ncartones){
			ordenarcarton(jugador[i].cartones[j]);
		}
	}
}	
procedure cartones(ref jugador: TipoJugadores)
	i: int;
	j: int;
{
	for(i=1, i <= Numjugadores){
		write("Jugador ");
		write(i);
		for(j=1,j <= jugador[i].ncartones){
			write(" Carton ");
			writeln(j);
			ordenarcarton(jugador[i].cartones[j]);
		}
	}
}





/*
procedure resultados(ref jugador: TipoJugadores, bolas: TipoBola)
	i: int;
	j: int;
{
	for(i=1, i <= Numjugadores){
		write("Jugador ");
		write(i);
		write(": ");


		estadocartones(jugador[i],bolas);
		writeln(".");
	}
}
*/
/*
procedure ganador(ref jugador: TipoJugadores, extraccion: TipoExtraccion, ref sale: bool)
	i: int;
	ganadores: int;
	sale: bool;
{
	ganadores=0;
	for(i=1, i <= Numjugadores){
		();
	}




	if(bingodeljugador(jugador[i])){;
		write("Ganador: ");
		write("Jugador ");
		writeln(i);
	}else if(bingodeljugador(jugador[i]) and bingodeljugador(jugador[j]) and bingodeljugador(jugador[k])){
		writeln("Empate");
	}else if(eof() and not ganado(jugador[i])){
		writeln("Empate");
	}
}
*/
procedure escribirbola(ref bola: TipoBola)
{
		write(bola.coloor);
		write(Blanco);
		writeln(bola.numeros);
}
procedure escribirextracciones(ref extraccion: TipoExtraccion)
	i: int;
{
	extraccion.nbolas=0;
	for(i=1, i <= extraccion.nbolas){	
		escribirbola(extraccion.bolaas[i]);
		extraccion.nbolas = extraccion.nbolas+1;
	}
}



procedure main()
	carton: TipoCarton;
	jugadores: TipoJugadores;
	ntachados: int;
	nums: TipoNumsC;
	num: TipoColumna;
{
	/*leerpartida(jugadores); extraccion*/		
	/*bolas.columnas=0;*/
	/*leercarton(carton);*/
	escribircarton(carton);
}

