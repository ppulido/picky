program bingo;

consts:
	MinJug=1;
	Numjugadores=3;
	Nfilas=3;
	Ncolumnas=5;
	Nummin=1;
	Nummax=100;
	Tachar="XX";
	Blanco=" ";
	Maxbolas=400;
	Maxcartones=400;
	
types:
	
	TipoNumFilas=int 1..Nfilas;
	TipoNumColumnas=int 1..Ncolumnas;
	TipoNumBolas=int 1..Maxbolas;
	TipoNumCartones=int 1..Maxcartones;

	TipoColumna = int Nummin..Nummax;
	TipoNumsC = array [TipoNumColumnas] of TipoColumna;
	TipoTachado= array [TipoNumColumnas] of bool;

	TipoEntrada=(Rojo, Verde, Azul, Amarillo, FIN);
	TipoColor= TipoEntrada Rojo..Amarillo;

	TipoBola = record
	{
		coloor: TipoColor;
		numeros: TipoColumna;
	};
	TipoBolas = array [TipoNumBolas] of TipoBola;
	TipoExtraccion = record
	{
		bolaas: TipoBolas;
		nbolas: int;
	};


	TipoFila = record
	{
		coloor: TipoColor;
		columnas: TipoNumsC;
		tachadoo: TipoTachado;
	};
	TipoCarton = array [TipoNumFilas] of TipoFila;
	TipoCartones = array [TipoNumCartones] of TipoCarton;
	TipoJugador = record
	{
		cartones: TipoCartones;
		ncartones: int;
	};
	TipoJugadores = array [MinJug..Numjugadores] of TipoJugador;


function espblanc(car: char): bool
{
	return car == Eol or car == ' ' or car == '	';
}
procedure saltarblancos(ref fichero: file)
	car: char;
	fin: bool;
{
	fin=False;
	while(not fin){
		fpeek(fichero,car);
		switch(car){
		case Eol:
			freadeol(fichero);
		case Eof:
			fin=True;
		default:
			if(espblanc(car)){
				fread(fichero,car);
			}else{
				fin=True;
			}
		}
	}
}
procedure leerentrada(ref fichero: file,ref entrada: TipoEntrada)
{
	saltarblancos(fichero);
	fread(fichero,entrada);
}
procedure leercolumnas(ref fichero: file,ref fila: TipoFila)
	i: int;
	num: int;
{
	for(i=1, i <= Ncolumnas){
		saltarblancos(fichero);
		fread(fichero,num);
		fila.columnas[i]=num;
	}
}
procedure leerfilas(ref fichero: file,ref cart: TipoCarton, ref e: TipoEntrada)
	i: int;
{
	cart[1].coloor=e;
	leercolumnas(fichero,cart[1]);
	for(i=2, i <= Nfilas){
		saltarblancos(fichero);
		fread(fichero,cart[i].coloor);
		leercolumnas(fichero,cart[i]);
	}
}
procedure leercarton(ref fichero: file,ref cart: TipoCarton, ref e: TipoEntrada)
	i: int;
{
	leerentrada(fichero,e);
	if(e != FIN){
		leerfilas(fichero,cart,e);
	}
}
procedure leercartones(ref fichero: file,ref jugador: TipoJugador)
	car: char;
	e: TipoEntrada;
	cart: TipoCarton;
{
	jugador.ncartones = 0;
	do{
		fpeek(fichero,car);
		if(car!=Eof){
			leercarton(fichero,cart,e);
			if(e != FIN){
				jugador.ncartones = jugador.ncartones+1;
				jugador.cartones[jugador.ncartones] = cart;
			}
		}
	}while(car!=Eof and e != FIN);
}
procedure leerpartida(ref fichero: file,ref jugador: TipoJugadores)
	i: int;
{
	for(i=1, i <= Numjugadores){
		leercartones(fichero,jugador[i]);
	}
}
procedure leerextraccion(ref fichero: file,ref bola: TipoBola)
	car: char;
	i: int;
{
	if(not eof()){
		saltarblancos(fichero);
		fpeek(fichero,car);
		fread(fichero,bola.coloor);
	}
	if(not eof()){
		saltarblancos(fichero);
		fpeek(fichero,car);
		fread(fichero,bola.numeros);
	}
}
procedure tacharfila(ref fila: TipoFila, bola: TipoBola,ref tachado: bool)
	i: int;
{
	for(i=1, i <= Ncolumnas){
		if(fila.coloor==bola.coloor and fila.columnas[i]==bola.numeros){
			tachado=True;
			fila.tachadoo[i]=True;
		}else{
			tachado=False;
			fila.tachadoo[i]=False;
		}
	}	
}
procedure tacharcarton(ref carton: TipoCarton, bola: TipoBola)
	i: int;
	tachado: bool;
{
	for(i=1, i <= Nfilas){
		tacharfila(carton[i],bola,tachado);
	}
}
procedure tacharcartones(ref jugador: TipoJugadores, bola: TipoBola)
	i: int;
	j: int;
{
	for(i=1,i <= Numjugadores){
		for(j=1,j <= jugador[i].ncartones){
			tacharcarton(jugador[i].cartones[j],bola);
		}
	}
}
function linea(fila: TipoFila, esta: bool): bool
	i: int;		
{
	esta=True;
	i=1;
	while(i <= Ncolumnas and esta){
		if(not fila.tachadoo[i]){
			esta=False;	
		}else{
			esta=True;
			i=i+1;
		}
	}
	return esta;
}
function bingocarton(cart: TipoCarton, esta: bool): bool
	i: int;
{
	esta=True;
	i=1;
	while(i <= Nfilas and esta){
		if(not linea(cart[i],esta)){
			esta=False;	
		}else{
			esta=True;
			i=i+1;
		}
	}
	return esta;
}
function bingodeljugador(jugador: TipoJugador, sale: bool): bool
	i: int;
{
	sale=False;
	i=1;
	while(i <= jugador.ncartones and not sale){
		if(bingocarton(jugador.cartones[i],sale)){
			sale=True;
			i=i+1;	
		}else{
			sale=False;
		}
	}
	return sale;
}
function ganador(jugador: TipoJugadores, esta: bool): bool
	i: int;
	j: int;
{
	i=1;
	for(i=1,i <= Numjugadores){
		if(bingodeljugador(jugador[i],esta)){
			esta=True;	
		}else{
			esta=False;
		}
	}
	return esta;
}





procedure escribircolumnas(fila: TipoFila)
	i: int;
{
	for(i=1, i <= Ncolumnas){
		if(fila.tachadoo[i]){
			write(Tachar);
		}else{
			write(fila.columnas[i]);
		}
	write(Blanco);
	}
}
procedure escrcarton(cart:TipoCarton)
	i: int;
{
	for(i=1, i <= Nfilas){
		write(cart[i].coloor);
		write(Blanco);
		escribircolumnas(cart[i]);
		writeeol();
	}
}
procedure escribircartones(jugador: TipoJugador)
	i: int;
{
	for(i = 1, i <= jugador.ncartones) {
		escrcarton(jugador.cartones[i]);
		writeeol();
	}
}
procedure escribirjugadores(jugadores: TipoJugadores)
	i: int;
{
	for(i = 1, i <= Numjugadores){
		escribircartones(jugadores[i]);
	}
}
procedure intercambiarnums(ref num1: TipoColumna, ref num2: TipoColumna)
	aux: TipoColumna;
{
	aux=num1;
	num1=num2;
	num2=aux;
}
procedure ordenarnumeros(ref num: TipoNumsC)
	i: int;
	j: int;
	min: TipoColumna;
{	
	min=num[1];
	for(i=1, i < Ncolumnas){
		for(j=1, j <= Ncolumnas){
			if(num[j] < min){	
				intercambiarnums(num[j],num[i]);
			}
		}
	}
}
procedure ordenarcarton(ref carton: TipoCarton)
	i: int;
{
	for(i=1, i <= Nfilas){
		ordenarnumeros(carton[i].columnas);
	}
	writeeol();
}
procedure ordenarcartones(ref jugador: TipoJugadores)
	i: int;
	j: int;
{
	for(i=1,i <= Numjugadores){
		for(j=1,j <= jugador[i].ncartones){
			ordenarcarton(jugador[i].cartones[j]);
		}
	}
}	
procedure cartones(ref jugador: TipoJugadores)
	i: int;
	j: int;
{
	for(i=1, i <= Numjugadores){
		write("Jugador ");
		write(i);
		for(j=1,j <= jugador[i].ncartones){
			write(" Carton ");
			writeln(j);
			ordenarcarton(jugador[i].cartones[j]);
		}
	}
}






procedure escribirbola(ref bola: TipoBola)
{
		write(bola.coloor);
		write(Blanco);
		writeln(bola.numeros);
}
procedure escribirextracciones(ref extraccion: TipoExtraccion)
	i: int;
{
	extraccion.nbolas=0;
	for(i=1, i <= extraccion.nbolas){	
		escribirbola(extraccion.bolaas[i]);
		extraccion.nbolas = extraccion.nbolas+1;
	}
}

procedure main()
	jugadores: TipoJugadores;
	e:TipoEntrada;
	carton: TipoCarton;
	fichero: file;
	bola: TipoBola;
{
	open(fichero, "datos.txt", "r");
	leerpartida(fichero,jugadores);
	close(fichero);
	escribirjugadores(jugadores);
	open(fichero, "datos.txt", "r");
	leerextraccion(fichero,bola);
	close(fichero);
}
