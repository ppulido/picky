#! /bin/bash

NETGUI_VERSION=0.4.10

HAS_UBUNTU=`cat /etc/issue | cut -f 1 -d " " | grep -i ubuntu`
HAS_LINUXMINT=`cat /etc/issue | cut -f 2 -d " " | grep -i mint`
UBUNTU_VERSION=$((`cat /etc/issue | cut -f 2 -d " " | cut -f1 -d "."`))
UBUNTU_MINORVERSION=$((`cat /etc/issue | cut -f 2 -d " " | cut -f2 -d "."`))
LINUXMINT_VERSION=$((`cat /etc/issue | cut -f 3 -d " " | cut -f1 -d "."`))
LINUXMINT_MINORVERSION=$((`cat /etc/issue | cut -f 3 -d " " | cut -f2 -d "."`))
HAS_64BITS=`uname -a |grep x86_64`


if [ ! -z "$HAS_UBUNTU" -a $UBUNTU_VERSION -gt 8 ]; then
	AUTO=OK
elif [ ! -z "$HAS_LINUXMINT" -a $LINUXMINT_VERSION -ge 17 ]; then
	AUTO=OK
fi

if [ ! -z "$AUTO" ]; then
	sudo apt-get update
	if [ ! -z "$HAS_UBUNTU" -a $UBUNTU_VERSION -ge 16 ]; then
		sudo apt-get -y install default-jre
		if [ ! -z "$HAS_64BITS" ]; then
			sudo apt-get -y install lib32z1 lib32ncurses5 libbz2-1.0:i386
			sudo apt-get -y install libc6-i386 lib32readline6
		fi
        	sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -q wireshark
        elif [ ! -z "$HAS_LINUXMINT" -a $LINUXMINT_VERSION -ge 18 ]; then
		sudo apt-get -y install default-jre
		if [ ! -z "$HAS_64BITS" ]; then
			sudo apt-get -y install lib32z1 lib32ncurses5 libbz2-1.0:i386
			sudo apt-get -y install libc6-i386 lib32readline6
		fi
        	sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -q wireshark
	else	
		sudo apt-get -y install openjdk-6-jre
		sudo apt-get -y install openjdk-8-jre
		if [ -d /usr/lib/jvm ]; then
			if [ ! -e /usr/lib/jvm/default-java ]; then
				if [ -e /usr/lib/jvm/java-8-openjdk-amd64 ]; then
					ln -s /usr/lib/jvm/java-8-openjdk-amd64 /usr/lib/jvm/default-java
				elif [ -e /usr/lib/jvm/java-6-openjdk-amd64 ]; then
					ln -s /usr/lib/jvm/java-6-openjdk-amd64 /usr/lib/jvm/default-java
				fi
			fi
		fi
		if [ ! -z "$HAS_64BITS" ]; then
			sudo apt-get -y install ia32-libs libc6-i386 lib32readline6
			sudo apt-get -y install libc6-i386 lib32readline6
			sudo apt-get -y install lib32z1 lib32ncurses5 lib32bz2-1.0
			sudo apt-get -y install lib32bz2-1.0
			sudo apt-get -y install libbz2-1.0:i386
		fi
		sudo apt-get -y install wireshark
	fi
	sudo apt-get -y install xterm xwit telnetd pv
        cd /tmp
	rm -f netgui-${NETGUI_VERSION}.tar.bz2
	wget http://mobiquo.gsyc.es/netgui-${NETGUI_VERSION}/netgui-${NETGUI_VERSION}.tar.bz2
	cd /usr/local
	sudo rm -rf netkit
	pv /tmp/netgui-${NETGUI_VERSION}.tar.bz2 | sudo tar -xjSf -
	sudo ln -fs /usr/local/netkit/netgui/bin/netgui.sh /usr/local/bin
	sudo ln -fs /usr/local/netkit/netgui/bin/clean-netgui.sh /usr/local/bin
	sudo ln -fs /usr/local/netkit/netgui/bin/clean-vm.sh /usr/local/bin
	echo 
	echo "NetGUI autoinstall completed"
else
	echo "autoinstall only works in Ubuntu >= 9.04 or Linux Mint >= 17, sorry"
fi

