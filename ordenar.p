program coches;

consts:
	NumAuto=10;
types:
	TipoDeMarca=(Ferrari,Lambo,Aston);
	TipoTamanno= int 1..Maxint;/*cm**3*/
	TipoDeMotor=(Atmo,Turbo);
	TipoDeCoche=record
	{
		marca: TipoDeMarca;
		tamanno: TipoTamanno;
		motor: TipoDeMotor;
	};
	TipoDeAuto= array[1..NumAuto] of TipoDeCoche;

procedure leercoches(ref coche: TipoDeAuto)
	i: int;
{
	for(i=1,i<=NumAuto){
		readln(coche[i].marca);
		readln(coche[i].tamanno);
		readln(coche[i].motor);
	}
}
function calcularpotencia(coche1: TipoDeCoche): int
	i: int;
	potenciaa: int;
{
	if(coche1.motor==Turbo){
		potenciaa=(coche1.tamanno*2);
	}else if(coche1.motor==Atmo){
		potenciaa=coche1.tamanno;
	}
	return potenciaa;
}
procedure intercambiar(ref coche1: TipoDeAuto,ref coche2: TipoDeAuto)
	aux: TipoDeAuto;
{
	aux=coche1;
	coche1=coche2;
	coche2=aux;
}
procedure buscarmenorpot(ref coche: TipoDeAuto)
	i: int;
	min: TipoDeCoche;
{
	min.tamanno=calcularpotencia(coche[1]);
	for(i=1,i<len NumAuto){
		if(calcularpotencia(coche[i])<(min.tamanno)){
			min=coche[i];
		}
	}
}
procedure ordenar(ref coche: TipoDeAuto)
	i: int;
{
	for(i=1,i< len NumAuto-1){
		buscarmenorpot(coche[i]);
		if(calcularpotencia(coche[i])<calcularpotencia(coche[i+1])){
			intercambiar(coche[i],coche[i+1]);
		}
	}
}
